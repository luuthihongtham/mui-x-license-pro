import { LicenseScope } from '../utils/licenseScope';
import { LicensingModel } from '../utils/licensingModel';
export interface LicenseDetails {
    orderNumber: string;
    expiryDate: Date;
    scope?: LicenseScope;
    licensingModel?: LicensingModel;
}
export declare function generateLicense(details: LicenseDetails): string;
