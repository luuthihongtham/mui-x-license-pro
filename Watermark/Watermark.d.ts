/// <reference types="react" />
import { MuiCommercialPackageName } from '../useLicenseVerifier';
interface WatermarkProps {
    packageName: MuiCommercialPackageName;
    releaseInfo: string;
}
export declare function Watermark(props: WatermarkProps): JSX.Element | null;
export {};
