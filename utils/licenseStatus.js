var LicenseStatus;

(function (LicenseStatus) {
  LicenseStatus["NotFound"] = "NotFound";
  LicenseStatus["Invalid"] = "Invalid";
  LicenseStatus["Expired"] = "Expired";
  LicenseStatus["Valid"] = "Valid";
})(LicenseStatus || (LicenseStatus = {}));

export { LicenseStatus };