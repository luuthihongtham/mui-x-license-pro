export declare const LICENSING_MODELS: readonly ["perpetual", "subscription"];
export declare type LicensingModel = typeof LICENSING_MODELS[number];
