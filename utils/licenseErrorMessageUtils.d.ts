export declare function showInvalidLicenseError(): void;
export declare function showNotFoundLicenseError({ plan, packageName, }: {
    plan: string;
    packageName: string;
}): void;
export declare function showExpiredLicenseError(): void;
