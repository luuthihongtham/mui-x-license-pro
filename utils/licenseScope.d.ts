export declare const LICENSE_SCOPES: readonly ["pro", "premium"];
export declare type LicenseScope = typeof LICENSE_SCOPES[number];
