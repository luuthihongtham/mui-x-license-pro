"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LicenseStatus = void 0;
var LicenseStatus;
exports.LicenseStatus = LicenseStatus;

(function (LicenseStatus) {
  LicenseStatus["NotFound"] = "NotFound";
  LicenseStatus["Invalid"] = "Invalid";
  LicenseStatus["Expired"] = "Expired";
  LicenseStatus["Valid"] = "Valid";
})(LicenseStatus || (exports.LicenseStatus = LicenseStatus = {}));