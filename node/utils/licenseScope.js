"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LICENSE_SCOPES = void 0;
const LICENSE_SCOPES = ['pro', 'premium'];
exports.LICENSE_SCOPES = LICENSE_SCOPES;