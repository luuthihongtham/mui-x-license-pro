"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.showExpiredLicenseError = showExpiredLicenseError;
exports.showInvalidLicenseError = showInvalidLicenseError;
exports.showNotFoundLicenseError = showNotFoundLicenseError;

function showError(message) {
  console.error(['*************************************************************', '', ...message, '', '*************************************************************'].join('\n'));
}

function showInvalidLicenseError() {
  showError(['MUI: Invalid license key.', '', "Your MUI X license key isn't valid. Please check your license key installation https://mui.com/r/x-license-key-installation.", '', 'To purchase a license, please visit https://mui.com/r/x-get-license.']);
}

function showNotFoundLicenseError({
  plan,
  packageName
}) {
  showError([`MUI: License key not found for ${packageName}.`, '', `This is a trial-only version of MUI X ${plan}.`, 'See the conditons here: https://mui.com/r/x-license-trial.', '', 'To purchase a license, please visit https://mui.com/r/x-get-license.']);
}

function showExpiredLicenseError() {
  showError(['MUI: License key expired.', '', 'Please visit https://mui.com/r/x-get-license to renew your subscription of MUI X.']);
}