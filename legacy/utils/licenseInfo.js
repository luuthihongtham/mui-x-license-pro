import _classCallCheck from "@babel/runtime/helpers/esm/classCallCheck";
import _createClass from "@babel/runtime/helpers/esm/createClass";
import { ponyfillGlobal } from '@mui/utils';
// Store the license information in a global, so it can be shared
// when module duplication occurs. The duplication of the modules can happen
// if using multiple version of MUI X at the same time of the bundler
// decide to duplicate to improve the size of the chunks.
// eslint-disable-next-line no-underscore-dangle
ponyfillGlobal.__MUI_LICENSE_INFO__ = ponyfillGlobal.__MUI_LICENSE_INFO__ || {
  key: undefined
};
export var LicenseInfo = /*#__PURE__*/function () {
  function LicenseInfo() {
    _classCallCheck(this, LicenseInfo);
  }

  _createClass(LicenseInfo, null, [{
    key: "getLicenseInfo",
    value: function getLicenseInfo() {
      // eslint-disable-next-line no-underscore-dangle
      return ponyfillGlobal.__MUI_LICENSE_INFO__;
    }
  }, {
    key: "getLicenseKey",
    value: function getLicenseKey() {
      return LicenseInfo.getLicenseInfo().key;
    }
  }, {
    key: "setLicenseKey",
    value: function setLicenseKey(key) {
      var licenseInfo = LicenseInfo.getLicenseInfo();
      licenseInfo.key = key;
    }
  }]);

  return LicenseInfo;
}();