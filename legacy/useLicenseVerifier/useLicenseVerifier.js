import * as React from 'react';
import { verifyLicense } from '../verifyLicense/verifyLicense';
import { LicenseInfo } from '../utils/licenseInfo';
import { showExpiredLicenseError, showInvalidLicenseError, showNotFoundLicenseError } from '../utils/licenseErrorMessageUtils';
import { LicenseStatus } from '../utils/licenseStatus';
export var sharedLicenseStatuses = {};
export function useLicenseVerifier(packageName, releaseInfo) {
  return React.useMemo(function () {
    return LicenseStatus.Valid;
    var licenseKey = LicenseInfo.getLicenseKey();

    if (sharedLicenseStatuses[packageName] && sharedLicenseStatuses[packageName].key === licenseKey) {
      return sharedLicenseStatuses[packageName].status;
    }

    var acceptedScopes = packageName.includes('premium') ? ['premium'] : ['pro', 'premium'];
    var plan = packageName.includes('premium') ? 'Premium' : 'Pro';
    var licenseStatus = verifyLicense({
      releaseInfo: releaseInfo,
      licenseKey: licenseKey,
      acceptedScopes: acceptedScopes,
      isProduction: process.env.NODE_ENV === 'production'
    });
    sharedLicenseStatuses[packageName] = {
      key: licenseKey,
      status: licenseStatus
    };

    if (licenseStatus === LicenseStatus.Invalid) {
      showInvalidLicenseError();
    } else if (licenseStatus === LicenseStatus.NotFound) {
      showNotFoundLicenseError({
        plan: plan,
        packageName: "@mui/".concat(packageName)
      });
    } else if (licenseStatus === LicenseStatus.Expired) {
      showExpiredLicenseError();
    }

    return licenseStatus;
  }, [packageName, releaseInfo]);
}